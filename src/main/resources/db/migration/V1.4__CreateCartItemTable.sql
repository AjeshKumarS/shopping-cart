CREATE TABLE Cart_Items(
    id int PRIMARY KEY,
    cart_id int,
    product_id int,
    quantity int NOT NULL,
    FOREIGN KEY(cart_id) REFERENCES Carts(id),
    FOREIGN KEY(product_id) REFERENCES Products(id)
);
