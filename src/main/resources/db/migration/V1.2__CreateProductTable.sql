CREATE TABLE Products(
    id int PRIMARY KEY,
    name varchar(25) NOT NULL,
    price decimal(10, 2) NOT NULL
);
