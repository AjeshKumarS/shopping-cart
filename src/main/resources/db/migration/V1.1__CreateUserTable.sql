CREATE TABLE Users(
    id int PRIMARY KEY,
    name varchar(25) NOT NULL,
    address varchar(100) NOT NULL
);