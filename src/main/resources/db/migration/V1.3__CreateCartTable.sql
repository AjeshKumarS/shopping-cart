CREATE TABLE Carts(
    id int PRIMARY KEY,
    user_id int,
    FOREIGN KEY(user_id) REFERENCES Users(id)
);
